import React from "react";

const PostGrid = props => {
  return (
    <div className="post-grid">
      <div className="wrapper">
        {
          props.info.map( function( { id, title, body, slug } ) {
            return (
              <div className="post-preview" key={id}>
                <h2>{title}</h2>
                <p>{
                  body.length > 100 ? 
                    `${body.substring( 0, 100 )}...` : 
                    body
                }</p>
                <a href={`/posts/${slug}`} className="btn">Read full post</a>
               
              </div>
                
            );
          } )
        }
      </div>  
    </div>
  );
};

export default PostGrid;
