import Head from "next/head";

const Header = () => {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta name="description" content="" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1"
        />
        <title>Oh, C.R.U.D.</title>
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>
      <header className="header">
        <div className="header-wrapper">
          <h1>Oh, C.R.U.D.</h1>
          <p className="subtitle">September 2021 React Challenge</p>
        </div>
      </header>
    </>
  );
};

export default Header;
