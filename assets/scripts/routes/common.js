export default {
  init() {
    $(".hamburger").on("click", function() {
		$(this).toggleClass("is-active");
		if ($(this).hasClass("is-active")) {
			$("body").addClass("menu-open");
		} else {
			$("body").removeClass("menu-open");
		}
	});
	$(window).resize(function() {
		$(".topnav").attr("style", "");
		$(".hamburger").removeClass("is-active");
	});

	$(window).on("scroll", function() {
		if ($(window).scrollTop() > 0) {
			$("header.header").addClass("fixed");
		} else {
			$("header.header").removeClass("fixed");
		}
	});

    /* SKIP LINKS */
    // skip link scroll to section
    const skipToAnchor = (aid) => {
        var aTag = $(aid);
        var focus = true;
        $('html,body').animate({
            scrollTop: aTag.offset().top
        }, 'slow');
        var first_child = $(aTag.children()[0]);
        var tag = first_child.prop('tagName').toLowerCase();

        if (tag !== 'a' && tag !== 'button' && tag !== 'input' && tag !== 'textarea') {
            if (first_child.attr('tabIndex') !== undefined) {
                first_child.attr('tabIndex', -1).focus();
                first_child.removeAttr('tabIndex');
            } else {
                first_child.focus();
            }
        } else {
            first_child.focus();
        }
    }

    // create skip links
    const skipLinks = () => {
        $('section').each(function () {
            const id = $(this).attr('id');
            if (id !== undefined) {
                // Use the section id to create a label for the items in the skip link list
                var sectionNameArray = id.split('-');
                var sectionName = '';
                for (var i = 0; i < sectionNameArray.length; i++) {
                    var str = sectionNameArray[i];
                    str = str.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                        return letter.toUpperCase();
                    });
                    sectionName += str;
                    if (i < sectionNameArray.length - 1) {
                        sectionName += " ";
                    }
                }
                var skiplink = "<li><a href='#" + id + "' class='text-link'>Skip to " + sectionName + "</a></li>";
                $('.skiplinks ul').append(skiplink);
            }
        });

        const skipLinkContainer = $('.skiplinks'),
            skipLink = $('.skiplinks a');

        skipLink.on('focus', function () {
            skipLinkContainer.addClass('show');
        });

        skipLink.on('blur', function () {
            skipLinkContainer.removeClass('show');
        });

        skipLink.on('click', function (e) {
            e.preventDefault();
            skipToAnchor($(this).attr('href'));
        });
    }

    skipLinks();

	// Scroll To
	(function() {
		$(".scrollto").on("click", function(e) {
			e.preventDefault();
			var section = $(this).attr("href");
			$("html, body").animate({ scrollTop: $(section).offset().top }, 1000);
		});
	})();

	// Form
	$("select").comboSelect();

	var sectionWaypoints = $("section").each(function() {
		$(this).waypoint({ handler: function(direction) {
				$(this.element).addClass("loaded");
			}, offset: "50%" });

		if ($(this).hasClass("scroll-group")) {
			console.log("adding scroll group waypoints");
			$(this).waypoint({ handler: function(direction) {
					if (direction === "down") {
						$(this.element).addClass("fixed");
					} else {
						$(this.element).removeClass("fixed");
					}
				}, offset: function() {
					return 100 + $("header.header").outerHeight();
				} });
			$(this).waypoint({ handler: function(direction) {
					if (direction === "down") {
						var offset = $(this.element)
								.find(".row:last-child")
								.offset().top - $(this.element).offset().top;
						$(this.element)
							.removeClass("fixed")
							.addClass("fixed-bottom");
						$(this.element)
							.find(".fixed-item")
							.css({ top: offset });
					} else {
						$(this.element)
							.addClass("fixed")
							.removeClass("fixed-bottom");
						$(this.element)
							.find(".fixed-item")
							.removeAttr("style");
					}
				}, offset: function() {
					return $(this.element).height() * -1 + $(this.element)
							.find(".row")
							.last()
							.outerHeight() + 100;
				} });
		}
	});

	var scrollGroupWaypoints = $("section.scroll-group .row-content").each(
		function() {
			$(this).waypoint({
				handler: function(direction) {
					$(this.element).addClass("loaded");
				},
				offset: "77%"
			});
		}
	);

	$("section.count")
		.find(".number")
		.each(function() {
			var options = {};
			var $num = $(this);
			var start = $(this).data("start");
			var end = $(this).data("end");
			options.prefix = $(this).data("prefix") ? $(this).data("prefix") : "";
			options.suffix = $(this).data("suffix") ? $(this).data("suffix") : "";
			options.separator = $(this).data("separator") ? $(this).data("separator") : ",";
			options.decimal = $(this).data("decimal") ? $(this).data("decimal") : ".";

			var countAnim = new CountUp($num.attr("id"), start, end, 0, 2, options);

			var countWp = $(this).waypoint({ handler: function(direction) {
					if (!countAnim.error) {
						countAnim.start();
					} else {
						console.log(countAnim.error);
					}
				}, offset: "50%" });
		});

	var lastWidth = 1440;

	$(window).on("load resize", function() {
		if ($(window).width() > 960 && lastWidth <= 960) {
			$("section.cta-section .right-image [mask]").attr("mask", "url(#mask-desktop)");
		} else if ($(window).width() <= 960 && lastWidth > 960) {
			$("section.cta-section .right-image [mask]").attr("mask", "url(#mask-tablet)");
		}
		lastWidth = $(window).width();
	});
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
