import Head from "next/head";
import Image from "next/image";
import Layout from "../components/Layout";
import PostGrid from "../components/PostGrid";
import axios from "axios";

export const getStaticProps = async() => {
  const { data } = await axios.get( "https://dev.june-react-workshop.sandbox7.cliquedomains.com/api/posts" );
    
  return {
    props: {
      data: data || {}
    },
    revalidate: 1
  };
};

export default function Home( props ) {
  console.log( props.data );
  return (
    <Layout>
      <PostGrid info={props.data}/>
    </Layout>
  );
}

