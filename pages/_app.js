// import '../assets/styles/globals.scss'
import "../assets/styles/main.scss";

function MyApp( { Component, pageProps } ) {
  return <Component {...pageProps} />;
}

export default MyApp;
