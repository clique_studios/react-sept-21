import React from "react";
import Layout from "../../components/Layout";
import axios from "axios";

export const getStaticPaths = async() => {
  const { data } = await axios.get( "https://dev.june-react-workshop.sandbox7.cliquedomains.com/api/posts" );

  const paths = data.map( post => {
    return {
      params: { slug: post.slug }
    };
  } );

  return {
    paths,
    fallback: false
  };
};

export const getStaticProps = async context => {
  const slug = context.params.slug;
  const res = await fetch( "https://dev.june-react-workshop.sandbox7.cliquedomains.com/api/posts/" + slug );
  const data = await res.json();

  return {
    props: { data }
  };
};

const IndivPost = props => {
  console.log( props.data );
  return (
    <div className="indiv-post">
      <div className="wrapper">
        <h1>{props.data.title}</h1>
        <p>{props.data.body}</p>
      </div>
     
    </div>
  );
};

export default IndivPost;
